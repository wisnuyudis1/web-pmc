$(document).ready(function(){
    $('#pasienbaru').on('submit',function(e) {
        $("#daftarPasienBaru").prop("disabled", true);
        $("#daftarPasienBaru").html("Sending...");
    if ($('#cpatchaTextBox').val() == code) {

        $.ajax({
            url:'source/add_baru.php', //===PHP file name====
            data:$(this).serialize(),
            type:'POST',
            success:function(response){
              var dataResult = JSON.parse(response);
            if (dataResult.statusCode==200){
            Swal.fire({
                icon: 'success',
                title: dataResult.noAntrian,
                text: dataResult.message
            }).then(function(){
                location.reload();
                });
            } else if (dataResult.statusCode==200) {
              Swal.fire({
                  icon: 'error',
                  title: 'Closed',
                  text: dataResult.message
              });
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: dataResult.message
                });
            }
            console.log(response);
            },
            error:function(response){
                Swal.fire({
                icon: 'error',
                title: 'Opps!',
                text: 'Server Error!'
                });
            }
        });
        e.preventDefault(); //This is to Avoid Page Refresh and Fire the Event "Click"
    } else {
        createCaptcha();
        document.getElementById('cpatchaTextBox').value = "";
        Swal.fire({
            icon: 'error',
            title: 'Opps!',
            text: 'Captcha Salah!'
            });
        e.preventDefault();
    }
    });


    $('#pasienlama').on('submit',function(e) {
      $("#daftarPasienLama").prop("disabled", true);
      $("#daftarPasienLama").html("Sending...");
      if ($('#cpatchaTextBox').val() == code) {

        $.ajax({
            url:'source/add_lama.php', //===PHP file name====
            data:$(this).serialize(),
            type:'POST',
            success:function(response){
            var dataResult = JSON.parse(response);
            if (dataResult.statusCode==200){
            Swal.fire({
                icon: 'success',
                title: dataResult.noAntrian,
                text: dataResult.message
            }).then(function(){
                location.reload();
                });
            } else if (dataResult.statusCode==405) {
              Swal.fire({
                  icon: 'error',
                  title: 'Closed',
                  text: dataResult.message
              });
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: dataResult.message
                });
            }
            },
            error:function(response){
                Swal.fire({
                icon: 'error',
                title: 'Opps!',
                text: 'Server Error!'
                });
            }
        });
        e.preventDefault(); //This is to Avoid Page Refresh and Fire the Event "Click"
    } else {
        createCaptcha();
        document.getElementById('cpatchaTextBox').value = "";
        Swal.fire({
            icon: 'error',
            title: 'Opps!',
            text: 'Captha Salah!'
            });
        e.preventDefault();
    }
    });

});
