<?php
header('Access-Control-Allow-Origin: *');
/* Database connection start */
$servername = "localhost";
$username = "u7138228_wp930";
$password = "Hesoyam21";
$dbname = "u7138228_db_web";

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());

/* Database connection end */


// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;
$columns = array( 
// datatable column index  => database column name
    0 => 'no_antrian',
    1 => 'norm', 
    2 => 'nama',
    3 => 'tgl_lahir',
    4 => 'alamat',
    5 => 'no_hp',
    6 => 'tgl_kunjung',
    7 => 'poli',
    8 => 'nama_dokter',
    9 => 'penjamin',
    10 => 'jenis',
    11 => 'status'  
);

// getting total number records without any search
$sql = "SELECT id, no_antrian, norm, nama, tgl_lahir, alamat, no_hp, tgl_kunjung, poli, nama_dokter, penjamin, jenis, status, tgl_input ";
$sql.=" FROM tb_pasien WHERE tgl_input LIKE '".date('Y-m-d')."'";
$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.

if( !empty($requestData['search']['value']) ) {
    // if there is a search parameter
    $sql = "SELECT id, no_antrian, norm, nama, tgl_lahir, alamat, no_hp, tgl_kunjung, poli, nama_dokter, penjamin, jenis, status, tgl_input ";
    $sql.=" FROM tb_pasien";
    $sql.=" WHERE tgl_input like '".date('Y-m-d')."' AND (no_antrian LIKE '%".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
    $sql.=" OR norm LIKE '%".$requestData['search']['value']."%' ";
    $sql.=" OR nama LIKE '%".$requestData['search']['value']."%' )";
    $query=mysqli_query($conn, $sql) or die(mysqli_error($conn));
    $totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start'].", ".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
    $query=mysqli_query($conn, $sql) or die(mysqli_error($conn)); // again run query with limit
} else {    
    $sql = "SELECT id, no_antrian, norm, nama, tgl_lahir, alamat, no_hp, tgl_kunjung, poli, nama_dokter, penjamin, jenis, status, tgl_input ";
    $sql.= " FROM tb_pasien WHERE tgl_input LIKE '".date('Y-m-d')."'";
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start'].", ".$requestData['length']."   ";
    
    $query=mysqli_query($conn, $sql) or die(mysqli_error($conn));
   
    
}

$data = array();
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
  if($row["status"]==1){$row["status"] = "Selesai";}else{$row["status"] = "Belum selesai";}
  if($row["status"]=="Belum selesai"){
    
      $button = "<td><button type='button' class='btn btn-primary update' data-id='".$row["id"]."'><i class='fas fa-check'></i></button></td>";
  } else {
    $button = '';
  }
    $nestedData=array(); 
    
    $nestedData[] = $row["no_antrian"];
    $nestedData[] = $row["norm"];
    $nestedData[] = $row["nama"];
    $nestedData[] = $row["tgl_lahir"];
    $nestedData[] = $row["alamat"];
    $nestedData[] = $row["no_hp"];
    $nestedData[] = $row["tgl_kunjung"];
    $nestedData[] = $row["poli"];
    $nestedData[] = $row["nama_dokter"];
    $nestedData[] = $row["penjamin"];
    $nestedData[] = $row["jenis"];
    $nestedData[] = $row["status"];
    $nestedData[] = $button;        
    
    $data[] = $nestedData;
    
}



$json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );

echo json_encode($json_data);  // send data as json format

?>