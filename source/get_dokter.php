<?php
header('Access-Control-Allow-Origin: *');
/* Database connection start */
$servername = "localhost";
$username = "u7138228_wp930";
$password = "Hesoyam21";
$dbname = "u7138228_db_web";

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());

/* Database connection end */


// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;
$columns = array( 
// datatable column index  => database column name
    0 => 'kode_poli',
    1 => 'nama_dokter', 
    2 => 's1',
    3 => 's2',
    4 => 's3',
    5 => 's4',
    6 => 's5',
    7 => 's6',
    8 => 's7',
    9 => 'status' 
);

// getting total number records without any search
$sql = "SELECT id, kode_poli, nama_dokter, s1, s2, s3, s4, s5, s6, s7, status ";
$sql.=" FROM dokter";
$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.

if( !empty($requestData['search']['value']) ) {
    // if there is a search parameter
    $sql = "SELECT id, kode_poli, nama_dokter, s1, s2, s3, s4, s5, s6, s7, status ";
    $sql.=" FROM dokter";
    $sql.=" WHERE kode_poli LIKE '%".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
    $sql.=" OR nama_dokter LIKE '%".$requestData['search']['value']."%' ";
    $query=mysqli_query($conn, $sql) or die(mysqli_error($conn));
    $totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start'].", ".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
    $query=mysqli_query($conn, $sql) or die(mysqli_error($conn)); // again run query with limit
} else {    
    $sql = "SELECT id, kode_poli, nama_dokter, s1, s2, s3, s4, s5, s6, s7, status ";
    $sql.= " FROM dokter ";
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start'].", ".$requestData['length']."   ";
    $query=mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

$data = array();
$no = 1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
  
  if($row["status"]==1){$row["status"] = "Aktif";}else{$row["status"] = "Tidak Aktif";}
  if($row["s1"]==1){$row["s1"] = "Ya";}else{$row["s1"] = "Tidak";}
  if($row["s2"]==1){$row["s2"] = "Ya";}else{$row["s2"] = "Tidak";}
  if($row["s3"]==1){$row["s3"] = "Ya";}else{$row["s3"] = "Tidak";}
  if($row["s4"]==1){$row["s4"] = "Ya";}else{$row["s4"] = "Tidak";}
  if($row["s5"]==1){$row["s5"] = "Ya";}else{$row["s5"] = "Tidak";}
  if($row["s6"]==1){$row["s6"] = "Ya";}else{$row["s6"] = "Tidak";}
  if($row["s7"]==1){$row["s7"] = "Ya";}else{$row["s7"] = "Tidak";}
  $id = $row['id'];
  $button = "<button type='button' class='btn btn-warning' data-toggle='modal' data-target='#editdokterModal$id'>
                <i class='fas fa-pencil-alt'></i>
            </button>";

    $nestedData=array(); 
    $nestedData[] = $no++;
    $nestedData[] = $row["kode_poli"];
    $nestedData[] = $row["nama_dokter"];
    $nestedData[] = $row["s1"];
    $nestedData[] = $row["s2"];
    $nestedData[] = $row["s3"];
    $nestedData[] = $row["s4"];
    $nestedData[] = $row["s5"];
    $nestedData[] = $row["s6"];
    $nestedData[] = $row["s7"];
    $nestedData[] = $row["status"];
    $nestedData[] = $button;        
    
    $data[] = $nestedData;
    
}

$json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );

echo json_encode($json_data);  // send data as json format

?>