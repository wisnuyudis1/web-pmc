<?php
//Include file koneksi ke database
include "config.php";
require_once('../sms/smsGatewayV4.php');
$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTYyMzM5NjE0MywiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjg4NDYwLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.AQUa7aWxMJcHISZ74SilF6feJKKtFaBDx14-sifFYaM";

 date_default_timezone_set('Asia/Jakarta');
$time = date('H');
//menerima nilai dari kiriman form daftar
$nama = $_POST['nama'];
$tgl_lahir = $_POST['tgl_lahir'];
$no_hp = $_POST['no_hp'];
$alamat = $_POST['alamat'];
$poli = $_POST['poli'];
$dokter = $_POST['dokter'];
$penjamin = $_POST['penjamin'];
$jenis = "Baru";
$date = date("Y-m-d");

$tgl = date('l');
if ($tgl=="Monday") { $tgl = 1;}
if ($tgl=="Tuesday") { $tgl = 2;}
if ($tgl=="Wednesday") { $tgl = 3;}
if ($tgl=="Thursday") { $tgl = 4;}
if ($tgl=="Friday") { $tgl = 5;}
if ($tgl=="Saturday") { $tgl = 6;}
if ($tgl=="Sunday") { $tgl = 7;}

$jam_dok = mysqli_query($kon, "SELECT s$tgl FROM jam_dokter WHERE nama_dokter ='$dokter' LIMIT 1");
$jam_ter = mysqli_fetch_array($jam_dok);
//$jam_dokter = $jam_ter['s'.$tgl];

$cek = mysqli_query($kon,"SELECT no_antrian FROM tb_pasien WHERE tgl_input = '$date' ORDER BY no_antrian DESC LIMIT 1 ");
$data = mysqli_fetch_array($cek);
$cek_no = mysqli_num_rows($cek);
if ($cek_no < 0 ) {
  $no_antrian = 1;
} else {
  $no_antrian = $data['no_antrian'];
  $no_antrian++;
}

$cekin = mysqli_query($kon,"SELECT * FROM tb_pasien WHERE nama='$nama' AND no_hp='$no_hp' AND tgl_lahir='$tgl_lahir' AND poli='$poli' AND tgl_input = '$date'");
$cekin_no = mysqli_num_rows($cekin);
if ($cekin_no > 0 ) {
  echo json_encode(array(
      "statusCode"=>401,
      "message" => "Anda telah mendaftar antrian online"
  ));
  exit;
} elseif ($time<6 || $time>18) {
  echo json_encode(array(
      "statusCode"=>405,
      "message" => "Antrian Online dibuka pukul 06.00 s/d 18.00"
  ));
  exit;
} else {
  $sql="INSERT INTO tb_pasien (no_antrian,nama,tgl_lahir,no_hp,alamat,poli,nama_dokter,penjamin,jenis) values ('$no_antrian','$nama','$tgl_lahir','$no_hp','$alamat','$poli','$dokter','$penjamin','$jenis')";
  $hasil=mysqli_query($kon,$sql);
  // initializing
//   $message = 'Anda mendapatkan no antrian M'.$no_antrian.'
//
// Silahkan tunjukkan nomor pendaftaran ini, atau SMS ini ke security kami.
//
// Terimakasih.
// RS PMC
// "Mitra sehat keluarga Priangan Utara"';
//   $deviceID = 124266;
//   $options = [];
//   // send sms
//   $smsGateway = new SmsGateway($token);
//   $result = $smsGateway->sendMessageToNumber($no_hp, $message, $deviceID, $options);
    echo json_encode(array(
        "statusCode"=>200,
        "noAntrian" => "M".$no_antrian,
        "message" => "Silakan screenshoot nomor antrian ini dan tunjukan ke Security kami."
    ));
  exit;
}

?>
