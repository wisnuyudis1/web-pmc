<?php include "source/config.php" ?>
<!doctypehtml>
<html lang=en>
   <meta charset=utf-8>
   <meta content="width=device-width,initial-scale=1"name=viewport>
   <title>Registrasi Antrian Online Pasien Baru</title>
   <meta content=""name=description>
   <meta content="Antrian Online Pasien Baru" name=title>
   <style>#exampleModal .modal-body{text-align:center;width:100%}</style>
   <link href=assets/img/pmc.png rel=icon>
   <link href=assets/img/apple-touch-icon.png rel=apple-touch-icon>
   <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"rel=stylesheet>
   <link href=assets/vendor/bootstrap/css/bootstrap.min.css rel=stylesheet>
   <link href=assets/vendor/icofont/icofont.min.css rel=stylesheet>
   <link href=assets/vendor/boxicons/css/boxicons.min.css rel=stylesheet>
   <link href=assets/vendor/venobox/venobox.css rel=stylesheet>
   <link href=assets/vendor/animate.css/animate.min.css rel=stylesheet>
   <link href=assets/vendor/remixicon/remixicon.css rel=stylesheet>
   <link href=assets/vendor/owl.carousel/assets/owl.carousel.min.css rel=stylesheet>
   <link href=assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css rel=stylesheet>
   <link href=assets/css/style.css rel=stylesheet>
   <script src=assets/vendor/jquery/jquery.min.js></script>
   <script src=assets/js/captcha.js></script>
   <style media="screen">
    #daftar[disabled=true] {
      background-color: #333 !important;
      }
  </style>
   <body onload=createCaptcha()>

      <div class="align-items-center d-lg-flex d-none fixed-top"id=topbar>
         <div class="container d-flex">
            <div class="mr-auto contact-info"><i class=icofont-envelope></i> <a href=mailto:contact@example.com>rumahsakitpmc@gmail.com</a> <i class=icofont-phone></i> (0260) 540033 <i class=icofont-google-map></i> Jl. Raya Rancasari Km. 4,35 Pamanukan-Subang</div>
            <div class=social-links><a href=https://www.facebook.com/rspamanukanmedicalcenter class=facebook><i class=icofont-facebook></i></a> <a href=https://www.instagram.com/rspamanukanmedicalcenter/ class=instagram><i class=icofont-instagram></i></a></div>
         </div>
      </div>
      <header class=fixed-top id=header>
         <div class="container d-flex align-items-center">
            <img src=assets/img/pmc.png height=60px width=60px>
            <h1 class="mr-auto logo"><a href=index.html><span style=color:#000>RS.</span> PAMANUKAN MEDICAL CENTER</a></h1>
            <nav class="d-none d-lg-block nav-menu">
               <ul>
                  <li><a href=index.html>Beranda</a>
                  <li><a href=index.html#about>Tentang</a>
                  <li><a href=index.html#services>Layanan</a>
                  <li><a href=index.html#infokes>Info Rumah Sakit</a>
                  <li><a href="galeri.php">Galeri</a></li>
                  <li><a href="karir.php">Karir</a></li>
                  <li><a href="#contact">Kontak</a></li>
               </ul>
            </nav>
            <button class="appointment-btn scrollto"type=button data-target=#exampleModal data-toggle=modal>Daftar Online</button>
         </div>
      </header>
      <br><br><br><br>
      <section id="galeri" class="gallery">
    <div class=container>
        <div class=section-title>
            <h2>Registrasi Antrian Pasien Baru</h2>
            <p>Harap masukan kelengkapan data dengan benar dan cermat.</div>
        </div>
        <script>
            // $(document).ready(function() {
            //     $('#poli').select2({
            //         placeholder: 'Pilih Poliklinik',
            //         language: "id"
            //     });
            //     $('#dokter').select2({
            //         placeholder: 'Pilih Dokter',
            //         language: "id"
            //     });
            // });
        </script>
        <div class="d-flex justify-content-center" style="background-color:#E9EBEC">
            <div class="col-lg-6">
                <form role="form" id="pasienbaru" method="post">
                    <fieldset>
                        <br>
                        <div class="form-group">
                            <label for="nama">Nama Lengkap</label>
                            <input class="form-control" placeholder="Masukan nama lengkap" name="nama" type="text" required>
                        </div>
                        <div class="form-group">
                            <label for="tgl_lahir">Tanggal lahir </label>
                            <input class="form-control" placeholder="tttt-bb-hh" name="tgl_lahir" type="text" required>
                        </div>
                        <div class="form-group">
                            <label for="no_hp">NO. HP</label>
                            <input class="form-control" name="no_hp" placeholder="Masukan Nomor HP Anda" type="text" required>
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea class="form-control" id="alamat" cols="30" rows="7" name="alamat" placeholder="Desa atau Kecamatan"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="poli">Klinik Tujuan</label>
                            <?php
                             date_default_timezone_set('Asia/Jakarta');
                                $tgl = date('l');
                                if ($tgl=="Monday") { $tgl = 1;}
                                if ($tgl=="Tuesday") { $tgl = 2;}
                                if ($tgl=="Wednesday") { $tgl = 3;}
                                if ($tgl=="Thursday") { $tgl = 4;}
                                if ($tgl=="Friday") { $tgl = 5;}
                                if ($tgl=="Saturday") { $tgl = 6;}
                                if ($tgl=="Sunday") { $tgl = 7;}

                                $sql = mysqli_query($kon,"SELECT * FROM poli WHERE `s$tgl`='1'");
                            ?>
                            <p>
                                <select class="form-control" name="poli" id="poli" required>
                                <option></option>
                                <?php
                                    while($poli = mysqli_fetch_assoc($sql)){
                                    echo '<option value="'.$poli['kode_poli'].'">'.$poli['nama_poli'].'</option>';
                                    }
                                ?>
                                </select>
                            </p>
                        </div>
                        <script>
                            $(document).ready(function() {
                                $("#poli").change(function(){
                                  var kode_poli = $(this).val();
                                  if ((kode_poli=="UM1") || (kode_poli=="UM2") || (kode_poli=="UM3")){
                                     $('#dokdok').hide();
                                     $("#dokter").prop("required", false);
                                     $('#penjamin option[value="BPJS"]').hide();
                                     $('#penjamin option[value="Mandiri InHealth"]').hide();
                                     $('#penjamin option[value="BNI Life"]').hide();
                                     $('#penjamin option[value="Admedika"]').hide();
                                     $('#penjamin option[value="Allianz"]').hide();
                                     $('#penjamin option[value="Asuransi Internasional SOS"]').hide();
                                     $('#penjamin').prop('selectedIndex',0);
                                  } else if (kode_poli=="UM0") {
                                    $('#dokdok').hide();
                                    $("#dokter").prop("required", false);
                                    $('#penjamin option[value="BPJS"]').show();
                                    $('#penjamin option[value="Mandiri InHealth"]').show();
                                    $('#penjamin option[value="BNI Life"]').show();
                                    $('#penjamin option[value="Admedika"]').show();
                                    $('#penjamin option[value="Allianz"]').show();
                                    $('#penjamin option[value="Asuransi Internasional SOS"]').show();
                                  } else {
                                    $('#dokdok').show();
                                    $("#dokter").prop("required", true);
                                    $('#penjamin option[value="BPJS"]').show();
                                    $('#penjamin option[value="Mandiri InHealth"]').show();
                                    $('#penjamin option[value="BNI Life"]').show();
                                    $('#penjamin option[value="Admedika"]').show();
                                    $('#penjamin option[value="Allianz"]').show();
                                    $('#penjamin option[value="Asuransi Internasional SOS"]').show();
                                  }
                                  $.ajax({
                                      type: "POST",
                                      dataType: "html",
                                      url: "source/dokter.php?jenis=dokter",
                                      data: "kode_poli="+kode_poli,
                                      success: function(msg){
                                          $("select#dokter").html(msg);
                                          // $("img#load1").show();
                                          // getAjaxKota();
                                      }
                                  });
                                });
                            });
                        </script>
                        <div class="form-group" id="dokdok">
                            <label for="dokter"><div id="dok">Dokter&nbsp;</div><div id="pesan"></div></label>
                            <p>
                                <select class="form-control" name="dokter" id="dokter" required>
                                <option></option>
                                </select>
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="penjamin">Penjamin</label>
                            <p>
                                <select class="form-control" name="penjamin" id="penjamin" required>
                                        <option value="UMUM">UMUM</option>
                                        <option value="BPJS">BPJS</option>
                                        <option value="Mandiri InHealth">Mandiri Inhealth</option>
                                        <option value="BNI Life">BNI Life</option>
                                        <option value="Admedika">Asuransi Admedika</option>
                                        <option value="Allianz">Asuransi Allianz</option>
                                        <option value="Asuransi Internasional SOS">Asuransi Internasional SOS</option>
                                </select>
                            </p>
                        </div>
                        <div id="captcha">
                        </div>

                        <input type="text" class="form-control" placeholder="Captcha" id="cpatchaTextBox" required>
                        <!-- <div class='mtcaptcha'></div> -->
                    </fieldset>
                    <br>
                    <button type="submit" class="btn btn-info" id="daftarPasienBaru">Daftar</button>
                    <br>
        <br>
        <br>
                </form>
            </div>
        </div>
    </div>

</section>
      <footer id=footer>
         <div class=footer-top>
            <div class=container>
               <div class=row>
                  <div class="col-lg-3 col-md-6 footer-contact">
                     <h3><strong>RS PMC</strong></h3>
                     <p>Jl. Raya Rancasari Km. 4,35 Pamanukan-Subang<br><br><strong>Telp: </strong>0260 540033<br><strong>Fax: </strong>0260 540088<br><strong>Email: </strong>rumahsakitpmc@gmail.com<br>
                  </div>
                  <div class="col-md-6 col-lg-2 footer-links">
                     <h4>Useful Links</h4>
                     <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href=index.html>Beranda</a>
                        <li><i class="bx bx-chevron-right"></i> <a href=index.html#services>Layanan</a>
                        <li><i class="bx bx-chevron-right"></i> <a href=index.html#infokes>Info Rumah Sakit</a>
                        <li><i class="bx bx-chevron-right"></i> <a href=galeri.php>Galeri</a>
                        <li><i class="bx bx-chevron-right"></i> <a href=index.html#contact>Hubungi Kami</a>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="container d-md-flex py-4">
            <div class="text-center mr-md-auto text-md-left">
               <div class=copyright>© Copyright <strong><span>IT RS PMC 2021</span></strong></div>
               <div class=credits></div>
            </div>
            <div class="social-links pt-3 pt-md-0 text-center text-md-right"><a href=https://www.facebook.com/rspamanukanmedicalcenter class=facebook><i class="bx bxl-facebook"></i></a> <a href=https://www.instagram.com/rspamanukanmedicalcenter class=instagram><i class="bx bxl-instagram"></i></a></div>
         </div>
      </footer>

      <div id=preloader></div>
      <a href=# class=back-to-top><i class=icofont-simple-up></i></a>

      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Anda mendaftar sebagai?</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
                  </div>
                  <div class="modal-body">
                      <div clas="sss">
                          <a type="button" class="btn btn-success" href="reg_pasienlama.php">
                              Pasien Lama
                          </a>
                          <a type="button" class="btn btn-primary" href="reg_pasienbaru.php">
                              Pasien Baru
                          </a>
                      </div>
                  </div>
                  <div class="container">
                      <p>
                  <h6>Pastikan anda sudah mengecek jadwal dokter terlebih dahulu sebelum melakukan pendaftaran online.</h6>
                      </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
              </div>
          </div>
      </div>
        

      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.2/dist/sweetalert2.all.min.js"></script>
      <script src=assets/vendor/bootstrap/js/bootstrap.bundle.min.js></script>
      <script src=assets/vendor/jquery.easing/jquery.easing.min.js></script>
      <script src=assets/vendor/php-email-form/validate.js></script>
      <script src=assets/vendor/venobox/venobox.min.js></script>
      <script src=assets/vendor/waypoints/jquery.waypoints.min.js></script>
      <script src=assets/vendor/counterup/counterup.min.js></script>
      <script src=assets/vendor/owl.carousel/owl.carousel.min.js></script>
      <script src=assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js></script>
      <script src="assets/js/app.js"></script>
      <script src=assets/js/main.js></script>
