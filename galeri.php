<!doctypehtml>
<html lang=en>
   <meta charset=utf-8>
   <meta content="width=device-width,initial-scale=1"name=viewport>
   <title>Galeri</title>
   <meta content="Gallery RS Pamanukan Medical Center"name=description>
   <meta content="Gallery RSPMC"name=keywords>
   <style>#exampleModal .modal-body{text-align:center;width:100%}</style>
   <link href=assets/img/pmc.png rel=icon>
   <link href=assets/img/apple-touch-icon.png rel=apple-touch-icon>
   <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"rel=stylesheet>
   <link href=assets/vendor/bootstrap/css/bootstrap.min.css rel=stylesheet>
   <link href=assets/vendor/icofont/icofont.min.css rel=stylesheet>
   <link href=assets/vendor/boxicons/css/boxicons.min.css rel=stylesheet>
   <link href=assets/vendor/venobox/venobox.css rel=stylesheet>
   <link href=assets/vendor/animate.css/animate.min.css rel=stylesheet>
   <link href=assets/vendor/remixicon/remixicon.css rel=stylesheet>
   <link href=assets/vendor/owl.carousel/assets/owl.carousel.min.css rel=stylesheet>
   <link href=assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css rel=stylesheet>
   <link href=assets/css/style.css rel=stylesheet>
   <script src=assets/js/captcha.js></script>
   <body>

      <div class="align-items-center d-lg-flex d-none fixed-top"id=topbar>
         <div class="container d-flex">
            <div class="mr-auto contact-info"><i class=icofont-envelope></i> <a href=mailto:contact@example.com>rumahsakitpmc@gmail.com</a> <i class=icofont-phone></i> (0260) 540033 <i class=icofont-google-map></i> Jl. Raya Rancasari Km. 4,35 Pamanukan-Subang</div>
            <div class=social-links><a href=https://www.facebook.com/rspamanukanmedicalcenter class=facebook><i class=icofont-facebook></i></a> <a href=https://www.instagram.com/rspamanukanmedicalcenter/ class=instagram><i class=icofont-instagram></i></a></div>
         </div>
      </div>
      <header class=fixed-top id=header>
         <div class="container d-flex align-items-center">
            <img src=assets/img/pmc.png height=60px width=60px>
            <h1 class="mr-auto logo"><a href=index.html><span style=color:#000>RS.</span> PAMANUKAN MEDICAL CENTER</a></h1>
            <nav class="d-none d-lg-block nav-menu">
               <ul>
                  <li><a href=index.html>Beranda</a>
                  <li><a href=index.html#about>Tentang</a>
                  <li><a href=index.html#services>Layanan</a>
                  <li><a href=index.html#infokes>Info Rumah Sakit</a>
                  <li class="active"><a href="galeri.php">Galeri</a></li>
                  <li><a href="karir.php">Karir</a></li>
                 <li><a href="index.html#contact">Kontak</a></li>
               </ul>
            </nav>
            <button class="appointment-btn scrollto"type=button data-target=#exampleModal data-toggle=modal>Daftar Online</button>
         </div>
      </header>
      <br><br><br><br>
      <section class=gallery id=galeri>
         <div class=container>
            <div class=section-title>
               <h2>Kegiatan Rumah Sakit</h2>
            </div>
         </div>
         <p style=text-align:left;margin-left:14px><b><mark style=background-color:#19cc67>Employee Gathering 2018</mark>
         <div class=container-fluid>
            <div class="row no-gutters">
               <div class="col-lg-12 col-md-4">
                  <div class=gallery-item><a href=assets/img/galeri/employee.jpg class=venobox data-gall=gallery-item style=width:100%><img src=assets/img/galeri/employee.jpg alt=""class=img-fluid></a></div>
               </div>
            </div>
         </div>
         <br>
         <p style=text-align:left;margin-left:14px><b><mark style=background-color:#19cc67>Kegiatan Family Gathering 2020</mark>
         <div class=container-fluid>
            <div class="row no-gutters">
               <div class="col-lg-12 col-md-4">
                  <div class=gallery-item><a href=assets/img/galeri/rspmc.jpg class=venobox data-gall=gallery-item><img src=assets/img/galeri/rspmc.jpg alt=""class=img-fluid></a></div>
               </div>
            </div>
         </div>
      </section>
      <footer id=footer>
         <div class=footer-top>
            <div class=container>
               <div class=row>
                  <div class="col-md-6 col-lg-3 footer-contact">
                     <h3><strong>RS PMC</strong></h3>
                     <p>Jl. Raya Rancasari Km. 4,35 Pamanukan-Subang<br><br><strong>Telp: </strong>0260 540033<br><strong>Fax: </strong>0260 540088<br><strong>Email: </strong>rumahsakitpmc@gmail.com<br>
                  </div>
                  <div class="col-md-6 col-lg-2 footer-links">
                     <h4>Useful Links</h4>
                     <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href=index.html>Beranda</a>
                        <li><i class="bx bx-chevron-right"></i> <a href=index.html#services>Layanan</a>
                        <li><i class="bx bx-chevron-right"></i> <a href=index.html#infokes>Info Rumah Sakit</a>
                        <li><i class="bx bx-chevron-right"></i> <a href=galeri.php>Galeri</a>
                        <li><i class="bx bx-chevron-right"></i> <a href=index.html#contact>Hubungi Kami</a>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="container d-md-flex py-4">
            <div class="text-center mr-md-auto text-md-left">
               <div class=copyright>© Copyright <strong><span>IT RS PMC 2021</span></strong></div>
               <div class=credits></div>
            </div>
            <div class="social-links pt-3 pt-md-0 text-center text-md-right"><a href=https://www.facebook.com/rspamanukanmedicalcenter class=facebook><i class="bx bxl-facebook"></i></a> <a href=https://www.instagram.com/rspamanukanmedicalcenter class=instagram><i class="bx bxl-instagram"></i></a></div>
         </div>
      </footer>
      <div id=preloader></div>
      <a href=# class=back-to-top><i class=icofont-simple-up></i></a>
      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Anda mendaftar sebagai?</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
                  </div>
                  <div class="modal-body">
                      <div clas="sss">
                          <a type="button" class="btn btn-success" href="reg_pasienlama.php">
                              Pasien Lama
                          </a>
                          <a type="button" class="btn btn-primary" href="reg_pasienbaru.php">
                              Pasien Baru
                          </a>
                      </div>
                  </div>
                  <div class="container">
                      <p>
                  <h6>Pastikan anda sudah mengecek jadwal dokter terlebih dahulu sebelum melakukan pendaftaran online.</h6>
                      </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
              </div>
          </div>
      </div>

      <script src=assets/vendor/jquery/jquery.min.js></script>
      <script src=assets/vendor/bootstrap/js/bootstrap.bundle.min.js></script>
      <script src=assets/vendor/jquery.easing/jquery.easing.min.js></script>
      <script src=assets/vendor/php-email-form/validate.js></script>
      <script src=assets/vendor/venobox/venobox.min.js></script>
      <script src=assets/vendor/waypoints/jquery.waypoints.min.js></script>
      <script src=assets/vendor/counterup/counterup.min.js></script>
      <script src=assets/vendor/owl.carousel/owl.carousel.min.js></script>
      <script src=assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js></script>
      <script src=assets/js/main.js></script>
