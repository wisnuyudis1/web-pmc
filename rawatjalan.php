<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Rawat Jalan </title>
  <meta content="Fasilitas Rawat Jalan RS Pamanukan Medical Center" name="description">
  <meta content="Rawat Jalan RSPMC" name="keywords">
  <meta content="Rawat Jalan RSPMC" name="title">
    <style>
        #exampleModal .modal-body {
            text-align: center;
            width: 100%;
        }
    </style>
  <!-- Favicons -->
  <link href="assets/img/pmc.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
   <script src="https://hcaptcha.com/1/api.js" async defer></script>
   
<script type="text/javascript" src="assets/js/captcha.js"></script>

  <!-- =======================================================
  * Template Name: Medilab - v2.1.0
  * Template URL: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body onload="createCaptcha()">

 <script>
    var mtcaptchaConfig = {
      "sitekey": "MTPublic-OixzdIBTo",
      "autoFormValidate": true
     };
   (function(){var mt_service = document.createElement('script');mt_service.async = true;mt_service.src = 'https://service.mtcaptcha.com/mtcv1/client/mtcaptcha.min.js';(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(mt_service);
   var mt_service2 = document.createElement('script');mt_service2.async = true;mt_service2.src = 'https://service2.mtcaptcha.com/mtcv1/client/mtcaptcha2.min.js';(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(mt_service2);}) ();
   </script>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i> <a href="mailto:contact@example.com">rumahsakitpmc@gmail.com</a>
        <i class="icofont-phone"></i> (0260) 540033
        <i class="icofont-google-map"></i> Jl. Raya Rancasari Km. 4,35 Pamanukan-Subang
      </div>
      <div class="social-links">
        <a href="https://www.facebook.com/rspamanukanmedicalcenter" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="https://www.instagram.com/rspamanukanmedicalcenter/" class="instagram"><i class="icofont-instagram"></i></a>
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
<img src="assets/img/pmc.png" width="60px"  height="60px"> &nbsp;
      <h1 class="logo mr-auto"><a href="index.html"><span style="color: black;">RS.</span> PAMANUKAN MEDICAL CENTER</a></h1>

      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="index.html">Beranda</a></li>
          <li><a href="index.html#about">Tentang</a></li>
          <li><a href="index.html#services">Layanan</a></li>
          <li><a href="index.html#infokes">Info Rumah Sakit</a></li>
                   <!--<li class="drop-down"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="drop-down"><a href="#">Deep Drop Down</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li> -->
            <li class="active"><a href="#galeri">Galeri</a></li>
          <li><a href="index.html#contact">Hubungi Kami</a></li>

        </ul>
      </nav><!-- .nav-menu -->
      <button type="button" class="appointment-btn scrollto" data-toggle="modal" data-target="#exampleModal">
    Daftar Online
    </button>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  
    
    <!-- ======= Frequently Asked Questions Section ======= -->
<br>
<br>
<br>
<br>

    <!-- ======= Gallery Section ======= -->
    <section id="galeri" class="gallery">

      <div class="container">

        <div class="section-title">
          <h2>Daftar Klinik Rumah Sakit Pamanukan Medical Center</h2>
 
        </div>
      </div>
   
      <div class="container-fluid">

        <div class="row no-gutters">

          <div class="col-lg-12 col-md-4">
            <div class="gallery-item" align="center">
              <a href="assets/img/Slide17.png" class="venobox" data-gall="gallery-item" style="width: 100%">
                <img src="assets/img/Slide17.png" alt="" class="img-fluid">
              </a>
            </div>
          </div>
        
          </div>

       </div>

      </div>
    </section><!-- End Gallery Section -->

    <!-- ======= Contact Section ======= -->
  
  <!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3><strong>RS PMC</strong></h3>
            <p>
              Jl. Raya Rancasari Km. 4,35 Pamanukan-Subang<br><br>
              <strong>Telp:&nbsp;</strong>0260 540033<br>
              <strong>Fax:&nbsp;</strong>0260 540088<br>
              <strong>Email:&nbsp;</strong>rumahsakitpmc@gmail.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="index.html">Beranda</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.html#services">Layanan</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.html#infokes">Info Rumah Sakit</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="galeri.php">Galeri</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.html#contact">Hubungi Kami</a></li>
            </ul>
          </div>
<!--
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>
 -->
        <!--  <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Join Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div> -->

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>IT RS PMC 2021</span></strong>
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/ -->

        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="https://www.facebook.com/rspamanukanmedicalcenter" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="https://www.instagram.com/rspamanukanmedicalcenter" class="instagram"><i class="bx bxl-instagram"></i></a>

      </div>
    </div>
  </footer><!-- End Footer -->

   <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Anda mendaftar sebagai?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
                </div>
                <div class="modal-body">
                    <div clas="sss">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#lamaModal">
                            Pasien Lama
                        </button>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#baruModal">
                            Pasien Baru
                        </button>
                    </div>
                </div>
                <div class="container">
                    <p>
                <h6>Pastikan anda sudah mengecek jadwal dokter terlebih dahulu sebelum melakukan pendaftaran online.</h6>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- LAMA -->
    <div class="modal fade" id="lamaModal" tabindex="-1" role="dialog" aria-labelledby="lamaModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="lamaModalLabel">Data identitas diri</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
                </div>
                <div class="modal-body">
                    <form action="source/add_lama.php" method="post" onsubmit="validateCaptcha()">
                        <fieldset>
                            <div class="form-group">
                                <label>No Rekam Medis</label>
                                <input class="form-control" name="norm" placeholder="Masukan Nomor Rekam Medis Anda" type="text">
                            </div>
                            <div class="form-group">
                                <label>Nama lengkap</label>
                                <input class="form-control" name="nama" placeholder="Masukan Nama Anda" type="text">
                            </div>
                            <div class="form-group">
                                <label>Tanggal lahir</label>
                                <input class="form-control" name="tgl_lahir" type="date">
                            </div>
                            <div class="form-group">
                                <label>Terakhir berkunjung</label>
                                <input class="form-control" name="tgl_kunjung" type="date">
                            </div>
                            <div class="form-group">
                                <label>Klinik Tujuan</label>
                                <p>
                                    <select class="form-control" name="poli">
                                            <option value="Poli Anak">Poli Anak</option>
                                            <option value="Poli Kandungan">Poli Kandungan</option>
                                            <option value="Poli Saraf">Poli Saraf</option>
                                            <option value="Poli Jantung">Poli Jantung</option>
                                            <option value="Poli Mata">Poli Mata</option>
                                            <option value="Poli Dalam">Poli Dalam</option>
                                            <option value="Poli THT">Poli THT</option>
                                            <option value="Poli Gigi">Poli Gigi</option>
                                            <option value="Poli Rehabilitas Medik">Poli Rehabilitas Medik</option>
                                            <option value="Poli Jiwa">Poli Jiwa</option>
                                            <option value="Poli Bedah">Poli Bedah</option>
                                            <option value="Fisio Terapi">Fisio Terapi</option>
                                        </select>
                                </p>
                            </div>
                            <div class="form-group">
                                <label>Penjamin</label>
                                <p>
                                    <select class="form-control" name="penjamin">
                                            <option value="UMUM">UMUM</option>
                                            <option value="BPJS">BPJS</option>
                                            <option value="Asuransi">Asuransi</option>
                                    </select>
                                </p>
                            </div>
                            <div id="captcha">
                            </div>
                            <input type="text" placeholder="Captcha" id="cpatchaTextBox"/>
<!-- <div class='mtcaptcha'></div> -->
                        </fieldset>
                        <div>

                        </div>

                </div>
                <div class="modal-footer">
                    <input type="submit" name="Submit" value="Submit" class="btn btn-info">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- BARU -->
    <div class="modal fade" id="baruModal" tabindex="-1" role="dialog" aria-labelledby="baruModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="baruModalLabel">Data identitas diri</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
                </div>
                <div class="modal-body">
                    <form action="source/add_baru.php" method="post">
                        <fieldset>
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input class="form-control" placeholder="Masukan nama lengkap" name="nama" type="text">
                            </div>
                            <div class="form-group">
                                <label>Tanggal lahir</label>
                                <input class="form-control" placeholder="" name="tgl_lahir" type="date">
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea class="form-control" id="alamat" cols="30" rows="7" name="alamat"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Klinik Tujuan</label>
                                <p>
                                    <select class="form-control" name="poli">
                                            <option value="Poli Anak">Poli Anak</option>
                                            <option value="Poli Kandungan">Poli Kandungan</option>
                                            <option value="Poli Saraf">Poli Saraf</option>
                                            <option value="Poli Jantung">Poli Jantung</option>
                                            <option value="Poli Mata">Poli Mata</option>
                                            <option value="Poli Dalam">Poli Dalam</option>
                                            <option value="Poli THT">Poli THT</option>
                                            <option value="Poli Gigi">Poli Gigi</option>
                                            <option value="Poli Rehabilitas Medik">Poli Rehabilitas Medik</option>
                                            <option value="Poli Jiwa">Poli Jiwa</option>
                                            <option value="Poli Bedah">Poli Bedah</option>
                                            <option value="Fisio Terapi">Fisio Terapi</option>
                                        </select>
                                </p>
                            </div>
                            <div class="form-group">
                                <label>Penjamin</label>
                                <p>
                                    <select class="form-control" name="penjamin">
                                            <option value="UMUM">UMUM</option>
                                            <option value="BPJS">BPJS</option>
                                            <option value="Asuransi">Asuransi</option>
                                    </select>
                                </p>
                            </div>
<div class='mtcaptcha'></div>
                        </fieldset>
                        <div>

                        </div>

                </div>
                <div class="modal-footer">
                    <input type="submit" name="Submit" value="Submit" class="btn btn-info">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>
